import { E2eTestsPage } from './app.po';

describe('e2e-tests App', () => {
  let page: E2eTestsPage;

  beforeEach(() => {
    page = new E2eTestsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
